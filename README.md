Schemat mocno uproszczony, bez masy, ani zasilania. Same kable sygnałowe. Do otworzenia w progamie http://fritzing.org/home/

Budowa i części opisane w temacie:
https://forbot.pl/forum/topic/12780-airsoftowy-gadzet-koparka-airsoftcoin/

Filmik z działania: https://www.youtube.com/watch?v=qoA_SEkDOQQ


Przy pierwszym uruchomieniu trzeba odkomentować linijkę 27 z pliku System.cpp, żeby wgrać podstawowe ustawienia. Następnie trzeba uruchomić urządzenie, zakomentować linijkę ponownie i wgrać program ponownie. Dopiero wtedy zapis ustawień będzie działał poprawnie. 

Licencja mit, więc można budować, przerabiać, a nawet sprzedawać.
