//
// Created by Krzysztof Lichocki on 16.11.2018.
//

#ifndef AIRSOFTCOUNTER_GAMETYPE_H
#define AIRSOFTCOUNTER_GAMETYPE_H

enum GameType{
    AIRSOFTCOIN=0,
    DOMINATION=1,
    RESPAWN=2
};

#endif //AIRSOFTCOUNTER_GAMETYPE_H
