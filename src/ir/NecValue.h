//
// Created by Krzysztof Lichocki on 16.11.2018.
//

#ifndef AIRSOFTCOUNTER_NEC_VALUES_H
#define AIRSOFTCOUNTER_NEC_VALUES_H
enum NecValue {
    NONE = 0,
    CH_MINUS = 41565,
    CH = 25245,
    CH_PLUS = 57885,
    PREVIOUS = 8925,
    NEXT = 765,
    PLAY_PAUSE = 49725,
    MINUS = 57375,
    PLUS = 43095,
    EQ = 36975,
    N0 = 26775,
    N100 = 39015,
    N200 = 45135,
    N1 = 12495,
    N2 = 6375,
    N3 = 31365,
    N4 = 4335,
    N5 = 14535,
    N6 = 23205,
    N7 = 17085,
    N8 = 19125,
    N9 = 21165
};
#endif //AIRSOFTCOUNTER_NEC_VALUES_H
